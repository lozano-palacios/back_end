from django.db import models

class Cliente(models.Model):
    id_cliente = models.AutoField(primary_key =True)
    nombre_cliente = models.CharField('Nombre del cliente', max_length=30)
    apellido_cliente = models.CharField('Apellido del cliente', max_length=30)
    celular_cliente =models.PositiveBigIntegerField(default=0)
    
    