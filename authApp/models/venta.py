from django.db import models
from .usuario import Usuario
from .producto import Producto
from .cliente import Cliente


class Venta(models.Model):
    id_venta = models.AutoField(primary_key =True)
    id_producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    id_cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    id_usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    fecha_venta = models.DateTimeField()
    total_descuento = models.DecimalField(max_digits = 10,decimal_places=3,default=0)
    total_venta = models.DecimalField(max_digits = 10, decimal_places=3,default=0)